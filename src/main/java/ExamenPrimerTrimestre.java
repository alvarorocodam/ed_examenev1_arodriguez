import java.util.Scanner;

public class ExamenPrimerTrimestre {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String palabra = "", lectura = "";
		float numero = 0, suma = 0, media = 0;
		int tama�o = 0, opcion = 0, contador = 0;
		boolean negativo = false;

		do {
			System.out.println("MENU");
			System.out.println("====");
			System.out.println("1. Palabra invertida");
			System.out.println("2. Tri�ngulo n�meros");
			System.out.println("3. Suma/Media");
			System.out.println("4. Salir \n");
			do {
				System.out.print("Introduce una opci�n: ");
				lectura = scan.nextLine();
				opcion = Integer.parseInt(lectura);
				if (opcion != 1 && opcion != 2 && opcion != 3 && opcion != 4) {
					System.out.println("Opci�n  incorrecta \n");
				}
			} while (opcion != 1 && opcion != 2 && opcion != 3 && opcion != 4);

			if (opcion == 1) {
				System.out.print("\nIntroduce una palabra: ");
				palabra = scan.nextLine().toUpperCase();
				for (int i = palabra.length() - 1; i >= 0; i--) {
					System.out.print(palabra.charAt(i));
				}
				System.out.println(" \n");
			} else if (opcion == 2) {

				do {
					System.out.print("\nIntroduce el tama�o del lado del tri�ngulo: ");
					lectura = scan.nextLine();
					tama�o = Integer.parseInt(lectura);
					if (tama�o <= 0) {
						System.out.println("El n�mero debe ser positivo \n");
					}
				} while (tama�o <= 0);

				for (int i = tama�o; i >= 1; i--) {
					for (int j = 1; j <= i; j++) {
						System.out.print(j + " ");
					}
					System.out.println(" ");
				}
				System.out.println(" \n");
			} else if (opcion == 3) {
				negativo = false;
				suma = 0;
				media = 0;
				contador = 0;

				do {
					System.out.println("\nIntroduce un n�mero (Negativo para salir)");
					lectura = scan.nextLine();
					numero = Float.parseFloat(lectura);
					if (contador == 0 && numero < 0) {
						do {
							System.out.println("No has introducido ning�n n�mero v�lido");
							lectura = scan.nextLine();
							numero = Float.parseFloat(lectura);
						} while (numero < 0);
					}
					if (numero < 0) {
						negativo = true;
					} else {
						suma += numero;
						contador += 1;
					}
				} while (negativo != true);

				media = suma / contador;
				System.out.println("La suma de los n�meros es: " + suma);
				System.out.println("La media de los n�meros es: " + media + "\n");

			} else if (opcion == 4) {
				System.out.println("\nSaliendo...");
			}
		} while (opcion != 4);
	}
}
